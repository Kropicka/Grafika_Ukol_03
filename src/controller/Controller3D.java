package controller;

import fill.ScanLine;
import model3D.Cube;
import model3D.Cuboid;
import model3D.Solid;
import model3D.Triangle;
import rasterize.FilledLineRasterizer;
import rasterize.Raster;
import render.WireframeEngine;
import transforms.*;
import view.Panel;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

public class Controller3D {

    //Potřebné proměné okna
    private final Panel panel;
    private final Raster raster;

    //Proměné wireframu a kamery
    private WireframeEngine wireframeMachine;
    private Camera camera;

    //Proměné vlastných objektů
    private List<Solid> listOfSolids = new ArrayList<>();
    private int chosenForTransforms = -1;

    //Držák pozice myšy (Pro otáčení
    private int mouseLastX = 0;
    private int mouseLastY = 0;

    public Controller3D (Panel panel) { //Konstruktor
     this.panel = panel;
     this.raster = panel.getRaster();

     wireframeMachine = new WireframeEngine(raster,new FilledLineRasterizer(raster)); //Definice wireframu
        //Nastavení kamery na pozici 15 X
    camera = new Camera()
                .withPosition(new Vec3D(15,0,0))
                .withAzimuth(Math.PI)
                .withZenith(0)
                .withFirstPerson(true);

    //Přidání kvádru (Nejsem si jistý jestli se počítá jako nové těleso, raději jsem udělal 3)
    listOfSolids.add(new Cube());

    //Přidání trojuhelníku a posun po ose Y
    listOfSolids.add(new Triangle());
    Mat4 m = listOfSolids.get(1).getModel().mul(new Mat4Transl(0, 2, 0));
    listOfSolids.get(1).setModelTransformation(m);

    //Přidání kvádru a posun po ose X
    listOfSolids.add(new Cuboid());
    Mat4 n = listOfSolids.get(2).getModel().mul(new Mat4Transl(2, 0, 0));
    listOfSolids.get(2).setModelTransformation(n);
    paintSolids();


    //Inicializační metoda
    initListeners();
    }

    private void initListeners() { //Inicializační metoda (Potřebná pro listenery

        panel.addMouseMotionListener(new MouseAdapter() {
            //Pohnutí myši

            @Override
            public void mousePressed(MouseEvent e) { //Zachycení pozice myšy
                mouseLastX = e.getX();
                mouseLastY = e.getY();
            }

            @Override
            public void mouseDragged(MouseEvent e) { //Odposlouchávání pro pohnutí myší (Znázornění vygenerování)

                //Otáčení kamery podle změny pozice myšy
                if ((e.getX() - mouseLastX) == -1) {
                    camera = camera.addAzimuth(Math.PI / 5000);
                }

                if ((e.getX() - mouseLastX) == 1) {
                    camera = camera.addAzimuth(-(Math.PI / 5000));
                }

                if ((e.getY() - mouseLastY) == -1) {
                    camera = camera.addZenith(Math.PI / 5000);
                }

                if ((e.getY() - mouseLastY) == 1) {
                    camera = camera.addZenith(-(Math.PI / 5000));
                }

                //Repaint plátna
                clear();
                paintSolids();

                //Zaznamenání poslední pozice
                mouseLastX = e.getX();
                mouseLastY = e.getY();
            }


        });

        panel.addKeyListener(new KeyAdapter() {

            @Override
            public void keyPressed(KeyEvent e) { //Adapter pro odposlouchávání
                double step = 0.3; //Definice kroku

                //Pohyb WASD
                if (e.getKeyCode() == KeyEvent.VK_A) {
                    camera =camera.left(step);
                }

                if (e.getKeyCode() == KeyEvent.VK_D) {
                    camera =camera.right(step);
                }

                if (e.getKeyCode() == KeyEvent.VK_W) {
                    camera =camera.up(step);
                }

                if (e.getKeyCode() == KeyEvent.VK_S) {
                    camera =camera.down(step);

                }

                //Přiblížení kamery
                if (e.getKeyCode() == KeyEvent.VK_Q) {
                    System.out.println("KEKW");
                    camera =camera.forward(1);

                }

                //Oddálení kamery
                if (e.getKeyCode() == KeyEvent.VK_E) {
                    camera =camera.backward(step);

                }


                //Translace tělesa
                if (chosenForTransforms !=-1) {
                    if (e.getKeyCode() == KeyEvent.VK_NUMPAD8) {
                        Mat4 m = listOfSolids.get(chosenForTransforms).getModel().mul(new Mat4Transl(-0.2, 0, 0));
                        listOfSolids.get(chosenForTransforms).setModelTransformation(m);
                    }

                    if (e.getKeyCode() == KeyEvent.VK_NUMPAD5) {
                        Mat4 m = listOfSolids.get(chosenForTransforms).getModel().mul(new Mat4Transl(0.2, 0, 0));
                        listOfSolids.get(chosenForTransforms).setModelTransformation(m);
                    }

                    if (e.getKeyCode() == KeyEvent.VK_NUMPAD4) {
                        Mat4 m = listOfSolids.get(chosenForTransforms).getModel().mul(new Mat4Transl(0, -0.2, 0));
                        listOfSolids.get(chosenForTransforms).setModelTransformation(m);

                    }

                    if (e.getKeyCode() == KeyEvent.VK_NUMPAD6) {
                        Mat4 m = listOfSolids.get(chosenForTransforms).getModel().mul(new Mat4Transl(0, 0.2, 0));
                        listOfSolids.get(chosenForTransforms).setModelTransformation(m);
                    }

                    if (e.getKeyCode() == 107) {
                        Mat4 m = listOfSolids.get(chosenForTransforms).getModel().mul(new Mat4Transl(0, 0, 0.2));
                        listOfSolids.get(chosenForTransforms).setModelTransformation(m);
                    }

                    if (e.getKeyCode() == 109) {
                        Mat4 m = listOfSolids.get(chosenForTransforms).getModel().mul(new Mat4Transl(0, 0, -0.2));
                        listOfSolids.get(chosenForTransforms).setModelTransformation(m);
                    }

                    //Rotace tělesa
                    if (e.getKeyCode() == KeyEvent.VK_NUMPAD1) {
                        Mat4 m = listOfSolids.get(chosenForTransforms).getModel().mul(new Mat4RotX(0.01));
                        listOfSolids.get(chosenForTransforms).setModelTransformation(m);
                    }

                    if (e.getKeyCode() == KeyEvent.VK_NUMPAD2) {
                        Mat4 m = listOfSolids.get(chosenForTransforms).getModel().mul(new Mat4RotY(0.01));
                        listOfSolids.get(chosenForTransforms).setModelTransformation(m);
                    }

                    if (e.getKeyCode() == KeyEvent.VK_NUMPAD3) {
                        Mat4 m = listOfSolids.get(chosenForTransforms).getModel().mul(new Mat4RotZ(0.01));
                        listOfSolids.get(chosenForTransforms).setModelTransformation(m);
                    }

                    //Zmenšení tělesa
                    if (e.getKeyCode() == 111) {
                        Mat4 m = listOfSolids.get(chosenForTransforms).getModel().mul(new Mat4Scale(0.9));
                        listOfSolids.get(chosenForTransforms).setModelTransformation(m);

                    }

                    //ZVětšení tělesa
                    if (e.getKeyCode() == 106) {
                        Mat4 m = listOfSolids.get(chosenForTransforms).getModel().mul(new Mat4Scale(1.1));
                        listOfSolids.get(chosenForTransforms).setModelTransformation(m);
                    }

                    //Návrat tělesa na bod 0
                    if (e.getKeyCode() == 110) {
                        listOfSolids.get(chosenForTransforms).setModelTransformation(new Mat4Identity());
                    }

                }

                //vybírání tělesa
                if (e.getKeyCode() == KeyEvent.VK_NUMPAD0) {
                    if(chosenForTransforms == -1){
                        chosenForTransforms = 0;
                    }else
                    if (chosenForTransforms < listOfSolids.size()){
                        chosenForTransforms++;
                    }else{chosenForTransforms = 0;}
                }


                clear();
                paintSolids();
                //repaint
            }


        });
    }

    public void clear() { //Metoda pro clear rasteru
        raster.clear();
    }

    private void paintSolids(){ //Vykreslení solidů
        Vec3D e = new Vec3D(10,0,0);
        Vec3D v = new Vec3D(-1,0,0);
        Vec3D u = new Vec3D(0,0,1);
        Mat4 view = new Mat4ViewRH(e,v,u);

        int c = 0;
        for (Solid s: listOfSolids)
        {

            wireframeMachine.setView(view);
            wireframeMachine.setView(camera.getViewMatrix());
            wireframeMachine.setProjection(new Mat4OrthoRH(6,4,0.1,30));
            if (c == chosenForTransforms){
                wireframeMachine.render(s, Color.WHITE);
            }else{
                wireframeMachine.render(s, Color.YELLOW);
            }

            c++;
        }



        wireframeMachine.renderAxis(); //Vykreslení osy
    }
}

