package model3D;

import transforms.Point3D;

import java.util.ArrayList;

public class Cuboid extends Solid {
    public Cuboid() {
        super();
        vertexBuffer.add(new Point3D(0, 0,0));
        vertexBuffer.add(new Point3D(1, 0,0));
        vertexBuffer.add(new Point3D(0, 2,0));
        vertexBuffer.add(new Point3D(1, 2,0));
        vertexBuffer.add(new Point3D(0, 0,1));
        vertexBuffer.add(new Point3D(1, 0,1));
        vertexBuffer.add(new Point3D(0, 2,1));
        vertexBuffer.add(new Point3D(1, 2,1));

        indexBuffer.add(0);indexBuffer.add(1);
        indexBuffer.add(0);indexBuffer.add(2);
        indexBuffer.add(1);indexBuffer.add(3);
        indexBuffer.add(2);indexBuffer.add(3);

        indexBuffer.add(4);indexBuffer.add(5);
        indexBuffer.add(4);indexBuffer.add(6);
        indexBuffer.add(5);indexBuffer.add(7);
        indexBuffer.add(6);indexBuffer.add(7);

        indexBuffer.add(0);indexBuffer.add(4);
        indexBuffer.add(1);indexBuffer.add(5);
        indexBuffer.add(2);indexBuffer.add(6);
        indexBuffer.add(3);indexBuffer.add(7);
    }
}
