package model3D;

import transforms.Point3D;

public class Triangle extends Solid {
    public Triangle(){
        super();

        vertexBuffer.add(new Point3D(0, 0,0));
        vertexBuffer.add(new Point3D(1, 0,0));
        vertexBuffer.add(new Point3D(0, 1,0));
        vertexBuffer.add(new Point3D(1, 1,0));
        vertexBuffer.add(new Point3D(0.5, 0.5,1));

        indexBuffer.add(0);indexBuffer.add(1);
        indexBuffer.add(0);indexBuffer.add(2);
        indexBuffer.add(1);indexBuffer.add(3);
        indexBuffer.add(2);indexBuffer.add(3);

        indexBuffer.add(4);indexBuffer.add(0);
        indexBuffer.add(4);indexBuffer.add(1);
        indexBuffer.add(4);indexBuffer.add(2);
        indexBuffer.add(4);indexBuffer.add(3);
    }
}
