package fill;

import misc.Sorter;
import model.Edge;
import model.Point;
import rasterize.FilledLineRasterizer;
import rasterize.Raster;

import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ScanLine implements Filler {

    //deklarace proměných

    //Rasterizovač úseček
    private FilledLineRasterizer lineCreator;

    //Deklarace barev
    private Color fillColor = Color.green;
    private Color borderColor = Color.yellow;

    //Deklarace bodů
    private List<Point> points;

    //Deklarace třídícího stroje (Vlastní implementace pro bonus)
    private Sorter sortMachine = new Sorter();

    //Proměná pro udrržení rasteru
    private Raster r;


    //Konstruktor
    public ScanLine(Raster r, Color bgrC, Color bordC, List<Point> seznamB){
        this.r = r;
        this.lineCreator = new FilledLineRasterizer(r);
        this.fillColor = bgrC;
        this.borderColor = bordC;
        this.points = seznamB;
    }


    //Zděděná metoda pro seedfill
    public void fill(Point p) {

    }


    //Fill metoda
    @Override
    public void fill() {

        //Výpis zadaných bodů
        System.out.println("Zadané bbody:");
        int pocitac = 1;
        for (Point k:points)
        {
            System.out.println("Bod:"+pocitac+" Souřadnice X: "+ k.getX() + " Souřadnice Y: " +k.getY());
            pocitac++;
        }


        //Deklarace proměné pro Maximum a minimum
        int minY = points.get(0).getY();
        int maxY = minY;

        //Deklarace hran
        List<Edge> edges = new ArrayList<>();
        Edge workedEdge;

        //Naplnění seznamu hran
        for (int i = 0; i < points.size(); i++){
            if (i+1 < points.size()){
                workedEdge =(new Edge(points.get(i),points.get(i+1)));
            }else{
                //Fix poslední hrany
                workedEdge =(new Edge(points.get(i),points.get(0)));
            }

            //Úprava hran
            if (!workedEdge.isHorizontal()){ //Odevzdání vodorovných
                workedEdge.orientation(); //Úprava orientace
                edges.add(workedEdge); //Konečné přidání

                // Přepočítání maxima a minima
                if (workedEdge.getY1() < minY){minY = workedEdge.getY1();}
                if (workedEdge.getY2() > maxY){maxY = workedEdge.getY2();}
            }
        }


        //Výpočet průsečíků
        List<Point> pruseciky = new ArrayList<>();
        List<Point> tempPruseciky = new ArrayList<>();

        for (int y = minY; y <= maxY; y++){ //Ciklus pro výšku pologonu
            for (int i =0; i< edges.size();i++){ //Ciklus pro každou hranu
                if(edges.get(i).hasIntersection(y)) //Kontrola průsečíku
                {
                    tempPruseciky.add(new Point(edges.get(i).getIntersection(y),y)); //Požadavek o prusečík
                }

            }

            sortMachine.sortPoints(tempPruseciky); //Strovnání průsečíků podle X
            pruseciky.addAll(tempPruseciky); //přídaní do držáku průsečíků
            tempPruseciky.clear(); //Vyčištění pomocné proměné


            tempPruseciky= new ArrayList<>();


        }

        for (int i = 0; i < pruseciky.size(); i += 2) { //Vykreslení průsečíků
            Point x1 = pruseciky.get(i);
            Point x2 = pruseciky.get(i +1) ;

            for (int x = x1.getX(); x <= x2.getX(); x++) {
                lineCreator.rasterize(x1.getX(),x1.getY(),x2.getX(),x2.getY(),Color.YELLOW);
            }

        }

        for (Edge e:edges) { //Obtažení kraje
            lineCreator.rasterize(e.getX1(),e.getY1(),e.getX2(),e.getY2(),borderColor);
        }
    }


}
