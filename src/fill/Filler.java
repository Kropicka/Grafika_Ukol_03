package fill;

import model.Point;

public interface Filler {

    void fill();
    void fill(Point p);

}
