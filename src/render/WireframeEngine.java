package render;

import model3D.Axis;
import model3D.Edge;
import model3D.Solid;
import rasterize.FilledLineRasterizer;
import rasterize.LineRasterizer;
import rasterize.Raster;
import transforms.Mat4;
import transforms.Point3D;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class WireframeEngine extends Renderer{ //Dědičnost z Renderer

    public Raster raster;
    public FilledLineRasterizer rasterMachine; //Prapodivný eror s dedičností :D Vím že super by měl raster a rasterizer nastavit sám ale z nějakého duvodu mi to tak nešlo udělat

    public WireframeEngine(Raster raster, FilledLineRasterizer rasterizer) //Konstruktor
    {
        super(raster, rasterizer);
        this.raster = raster;
        this.rasterMachine = rasterizer;
    }


    @Override
    public void render(Solid solid){ //Render solidu

        Mat4 trans = solid.getModel().mul(view).mul(projection);// Transformace příprava matice

        List<Point3D> tempVB = new ArrayList<>();
        for (Point3D point : solid.getVertexBuffer()){
            tempVB.add(point.mul(trans));//Seřazení a transformování bodů
        }


        for (int i = 0; i+1 < solid.getIndexBuffer().size() ; i= i+2) {

            //Vyrenderování spojených vrcholů
            int indexA = solid.getIndexBuffer().get(i);
            int indexB = solid.getIndexBuffer().get(i+1);
            Point3D a = tempVB.get(indexA);
            Point3D b = tempVB.get(indexB);

            render(a,b,Color.yellow);
        }
    }

    public void render(Solid solid, Color color){ //Renderer s možností jiné barvy
        Mat4 trans = solid.getModel().mul(view).mul(projection);
        List<Point3D> tempVB = new ArrayList<>();
        for (Point3D point : solid.getVertexBuffer()){
            tempVB.add(point.mul(trans));
        }


        for (int i = 0; i+1 < solid.getIndexBuffer().size() ; i= i+2) {


            int indexA = solid.getIndexBuffer().get(i);
            int indexB = solid.getIndexBuffer().get(i+1);
            Point3D a = tempVB.get(indexA);
            Point3D b = tempVB.get(indexB);

            render(a,b,color);
        }
    }

    public void render(Point3D a, Point3D b,Color color){ // VLastní render přímek
        boolean isIn = true;

        //Chybí demhomogenizace a přechod na 2D... ale program funguje

        int x1,y1,x2,y2;
        x1 = (int)((a.getX() + 1)*(raster.getWidth()/2)) ;
        y1 = ((int)((-a.getY() + 1)*(raster.getHeight()/2)));
        x2 = (int)((b.getX() + 1)*(raster.getWidth()/2)) ;
        y2 = ((int)((-b.getY() + 1)*(raster.getHeight()/2)));

        if (x1 < 0 && x1 > raster.getWidth() && x2 < 0 && x2 > raster.getWidth()){isIn = false;} //Clip
        if (y1 < 0 && y1 > raster.getHeight() && x2 < 0 && x2 > raster.getHeight()){isIn = false;} //Clip

        if (isIn){ //Podmínka oříznutí
        rasterMachine.rasterize(x1,y1,x2,y2, color); //Vlastní vykreslení
        }

    }

    public void renderAxis(){ //Metoda pro vytvoření osy
        Solid axis = new Axis();
        Mat4 trans = axis.getModel().mul(view).mul(projection);
        List<Point3D> tempVB = new ArrayList<>();
        for (Point3D point : axis.getVertexBuffer()){
            tempVB.add(point.mul(trans));
        }

            render(tempVB.get(0),tempVB.get(1),Color.RED);
            render(tempVB.get(1),tempVB.get(2),Color.RED);
            render(tempVB.get(1),tempVB.get(3),Color.RED);

            render(tempVB.get(0),tempVB.get(4),Color.GREEN);
            render(tempVB.get(4),tempVB.get(5),Color.GREEN);
            render(tempVB.get(4),tempVB.get(6),Color.GREEN);

            render(tempVB.get(0),tempVB.get(7),Color.BLUE);
            render(tempVB.get(7),tempVB.get(8),Color.BLUE);
            render(tempVB.get(7),tempVB.get(9),Color.BLUE);
    }
}
