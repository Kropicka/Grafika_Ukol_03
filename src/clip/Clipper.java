package clip;

import model.Line;
import model.Point;
import model.Polyline;

public class Clipper {
    //private Polyline sourcePoly; //orezavany (input)
    //private Polyline resultPoly; //orezany (output)
    //private Polyline clipPoly; //orezavaci

    public static Polyline clip (Polyline sourcePoly, Polyline clipPoly)
    {
        Polyline resultPoly = new Polyline();
        return resultPoly;
    }

    public Point calculateIntersection(Line A, Line B){
        int x1= A.getX1();
        int y1= A.getY1();
        int x2= A.getX2();
        int y2= A.getY2();
        int x3= B.getX1();
        int y3= B.getY1();
        int x4= B.getX2();
        int y4= B.getY2();

        if ((x1 == x2 && y1 == y2) || (x3 == x4 && y3 == y4)) {
        }

        int ua,ub;
        int denominator = (y4 - y3)*(x2 - x1) - (x4 - x3)*(y2 - y1);

        ua = ((x4 - x3) * (y1 - y3) - (y4 - y3) * (x1 - x3)) / denominator;
        ub = ((x2 - x1) * (y1 - y3) - (y2 - y1) * (x1 - x3)) / denominator;

        int x = x1 + ua * (x2 - x1);
        int y = y1 + ua * (y2 - y1);

        return new Point(x,y);
    }
}
