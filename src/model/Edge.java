package model;

public class Edge {

    //Deklarace proměných
    public int x1, x2, y1, y2;


    //Konstruktor
    public Edge(Point p1, Point p2) {
        this.x1 = p1.getX();
        this.y1 = p1.getY();
        this.x2 = p2.getX();
        this.y2 = p2.getY();
        orientation();
    }


    //Metoda pro zjištění horizontálních hran
    public boolean isHorizontal(){
        return y1 == y2;
    }

    //Přetoření orientace
    public  void orientation(){
        if (y1 > y2){

            int a = y1;
            y1 = y2;
            y2 = a;

            a = x1;
            x1 = x2;
            x2 = a;
        }

    }

    //zjištění průsečíků
    public boolean hasIntersection(int y){
        return ((y >= y1) && (y < y2)); //Ostrá podmínka pro zkrácení hrany
    }

    //Výpočet průsečíku
    public int getIntersection(int Y){

        double deltaX = x2-x1;
        double deltaY = y2-y1;
        double k = deltaX/deltaY;
        double q = x1 - k *y1;
        double x = k*Y+q; //Chyba pravděpodobně zde (?)
        return (int)x;

    }

    //gettery
    public int getX1() {
        return x1;
    }

    public int getX2() {
        return x2;
    }

    public int getY1() {
        return y1;
    }

    public int getY2() {
        return y2;
    }


}
